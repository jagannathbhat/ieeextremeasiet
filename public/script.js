let process;
let matrix = () => {
    const c = document.getElementById("matrix");
    const ctx = c.getContext("2d");
    const font_size = 12;
    let columns = 0;
    let drops = [];
    let text = "01011010110101010111000100";
    c.height = window.innerHeight;
    c.width = window.innerWidth;
    columns = c.width / font_size;
    text = text.split("");
    for (let x = 0; x < columns; x++)
        drops[x] = 1;
    process = setInterval(() => {
        ctx.fillStyle = "rgba(240, 240, 240, 0.05)";
        ctx.fillRect(0, 0, c.width, c.height);
        ctx.fillStyle = "#006699";
        ctx.font = font_size + "px arial";
        for (let i = 0; i < drops.length; i++) {
            let text0 = text[Math.floor(Math.random() * text.length)];
            ctx.fillText(text0, i * font_size, drops[i] * font_size);
            if (drops[i] * font_size > c.height && Math.random() > 0.975)
                drops[i] = 0;
            drops[i]++;
        }
    }, 33);
};

window.onload = () => {
    matrix();
}

window.onresize = () => {
    clearInterval(process);
    matrix();
}